CONFIGURE ENVIRONMENT

1. Import the environment file and collection file into Postman.
2. Clone the environment "Example IDN" and open it for editing.  Give the environment an appropriate tile.
3. For the variable [org_name], provide the name of your tenant org.  Do not enter any part of a URL, it will be calculated automatically.
4. If the org is a demo org, add an environment variable named "demo_org" with a value of "yes".
5. In the IDN UI, create a new PAT and copy the new values into the [pat_id] and [pat_secret] environment variables.
6. Use any non-altering call such as "Get Organizational Settings" to test the authentication.

OPERATION

- With every subsequent API call, the PAT will be used for access and refreshed automatically as needed.

- To troubleshoot, open up the Postman console and make some simple calls to see what's going on.

NOTES

- The script that handles the authentication and token refresh is a Pre-request script attached to the top folder of the collection.  It shouldn't be too hard to extract if needed to accomodate other collection styles.

- Warning: The generated access tokens are stored in the environment file for reuse while they're valid. The variable is not automatically persisted if the environment is exported but care should be taken to protect the value from inadvertant sharing.

Author: Greggulator